# ml4proflow-mods-openmodelica

A module for ml4proflow to simulate modelica-models (.mo) with the python interface of OpenModelica

[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/file/reports/junit/report.html?job=gen-cov)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/file/reports/coverage/index.html?job=gen-cov)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/file/reports/flake8/index.html?job=gen-cov)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)]()
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)]()
------------
## Usage 
To configure the module correctly, add the following module configuration to the configuration file:
```json5 
{
    // User settings
    "Initialization_Model Path": "path/to/model.mo",
    "Initialization_Model Name": "ModelName",

    "Simulation_Model Parameters" : {},
    "Simulation_Outputs" : ["output1","output2","..."],

    "Framework_Self Execution" : False,

    // Expert settings
    "Initialization_Dependent Libraries" : [],
    "Simulation_Model Inputs" : {},
    "Simulation_Simulation Options" : {},
    "Simulation_Simulation Flags" : {}
}
```

The user settings should be configurated by every user. To initialize the model, the model path (with .mo extension) and the model name should be defined. For the simulation, the parameter and the outputs of the model should be defined. Define wether the module should be at the beginning (enable self execution) or somewhere else in the graph. 
The expert settings should be conifgured only by experts.

Necessary keys:
- Model Path
- Model Name
- Ouputs (default: time)

## Prerequisites
- [OpenModelica](https://www.openmodelica.org/)
- [ml4proflow](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow)

## Installation
Activate your virtual environment, clone this repository and install the package with pip:
```console 
$ pip install .
```

## Contribution
```console 
$ pip install -e .
```
