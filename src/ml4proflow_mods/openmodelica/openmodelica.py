from __future__ import annotations
from typing import Any, List
import pandas as pd

from OMPython import OMCSessionZMQ
from OMPython import ModelicaSystem


class OMSim:

    def __init__(self, model_path: str,
                 model_name: str, dep_libs: list[str] = []):

        self.omc = OMCSessionZMQ()
        self.mod = ModelicaSystem(model_path, model_name, dep_libs)
        self.result_file = model_name+".mat"

        self.input_list: List[str] = []
        self.param_list: List[str] = []
        self.simOpt_list: List[str] = []
        self.sim_Flags: str = ""

    def OMCTalker(self, expression: str):
        """Helper function:
        Sending Commands to the OM-Compiler
        :param expression: Command as string
        :return: sendExpression from OMC
        """
        return self.omc.sendExpression(expression)

    def showInputs(self):
        return self.mod.getInputs()

    def showParams(self, params: list = None):
        return self.mod.getParameters(params)

    def showOutputs(self):
        return self.mod.getOutputs()

    def showSimOpts(self, sim_opts: list = None):
        return self.mod.getSimulationOptions(sim_opts)

    def simulate(self, def_out: list = ["time"], inputs: dict[str, Any] = {},
                 params: dict[str, Any] = {}, sim_opts: dict[str, Any] = {},
                 simFlags: dict[str, Any] = {}):
        """Helper function:
        Simulate Modelica-Model and get the output
        :param def_out: Output of the Modelica-Model
        (by default the Simulation Time)
        :param inputs:  Inputs of the Modelica-Model
        (default values stored in the Model)
        :param params:  Parameter of the Modelica-Model
        (default values stored in the Model)
        :param sim_opts:  Simulation-Options of the Modelica-Model
        (default values stored in the Model)
        :param simFlags:  C-Runtime Flags for the simulation
        (see OpenModelica-Documentation for more infos)
        :return: om_data
        """
        keys = list(inputs.keys())
        values = list(inputs.values())
        for i in keys:
            self.input_list.append(keys[i] + " = {}".format(values[i]))
        self.mod.setInputs(self.input_list)

        keys = list(params.keys())
        values = list(params.values())
        for i in keys:
            self.param_list.append(keys[i] + " = {}".format(values[i]))
        self.mod.setParameters(self.param_list)

        keys = list(sim_opts.keys())
        values = list(sim_opts.values())
        for i in keys:
            self.simOpt_list.append(keys[i] + " = {}".format(values[i]))
        self.mod.setSimulationOptions(self.simOpt_list)

        keys = list(simFlags.keys())
        values = list(simFlags.values())
        for i in keys:
            self.sim_Flags = self.sim_Flags + " " + keys[i]
            if values[i] is not None:
                self.sim_Flags = self.sim_Flags + "="+str(values[i])

        self.mod.simulate(resultfile=self.result_file, simflags=self.sim_Flags)
        sim_out = self.mod.getSolutions(def_out, resultfile=self.result_file)
        print(def_out)
        print(sim_out)
        om_data = pd.DataFrame(data=sim_out.transpose(), columns=def_out)

        return om_data
