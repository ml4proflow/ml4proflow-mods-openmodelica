from __future__ import annotations
from typing import Any, Union
import os
import pandas

from ml4proflow.modules import (Module, ExecutableModule,
                                DataFlowManager, SourceModule, BasicModule)
from .openmodelica import OMSim


class OMModule(Module, ExecutableModule, OMSim):
    """
    ml4proflow-module to perform simulations
    of modelica-models (.mo).
    This module receives
    from a previous module some simulation parameters
    (either inputs, parameters, options or flags)
    and pushes the output data from the simulation
    to the designated channel.
    """
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]) -> None:
        """
        Initialize the OMModule
        and converting the keys & values
        from the configuration file
        to simulation parameters.
        :param dataflow: Specifies the dataflow
        :param config: Configuration of the module
        :return: None
        """

        Module.__init__(self, dfm, config)
        ExecutableModule.__init__(self,dfm,config)
        # Initialization Parameter
        self.model_path = self.config.setdefault('Initialization_Model Path', os.getcwd())
        # self.config['Initialization_Model Path'].replace("\\", "/")
        self.model_path = self.model_path.replace("\\", "/")
        self.model_name = self.config.setdefault('Initialization_Model Name', "ml4proflow_OMTest")
        
        self.config.setdefault("Framework_Executable Module",
                                            True)

        # Simulation Parameter 
        self.config.setdefault("Simulation_Model Parameter", {})
        self.config.setdefault("Simulation_Outputs", ["time"])


        # Expert settings
        self.dep_libs = self.config.setdefault('Initialization_Dependent Libraries', [])
        self.config.setdefault("Simulation_Model Inputs", {})
        self.config.setdefault("Simulation_Simulation Options", {})
        self.config.setdefault("Simulation_Simulation Flags", {})

        # OMSim.__init__(self, self.config["Initialization_Model Path"], self.config["Initialization_Model Name"], self.config["Initialization_Dependent Libraries"])
        OMSim.__init__(self, self.model_path, self.model_name, self.dep_libs)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "Open Modelica Simulation",
                "categories": ["Simulation"],
                "jupyter-gui-cls": "ml4proflow_jupyter.widgets.BasicWidget",
                "jupyter-gui-config-hide": [
                             'Initialization_Dependent Libraries',
                             'Simulation_Model Inputs',
                             'Simulation_Simulation Options',
                             'Simulation_Simulation Flags'
                            ],
                "jupyter-gui-override-settings-type": {
                             'Initialization_Model Path': 'file',
                             'Initialization_Model Name': 'string',
                             'Simulation_Model Parameter': 'dict',
                             'Simulation_Outputs': 'list',
                             'Framework_Executable Module': 'bool'
                            },
                "html-description": """
                <p>This <strong>ml4proflow module</strong> simulates
                modelica-models (.mo) with the python interface of
                OpenModelica. It can bes used as
                <em>ExecutableModule</em> at the beginning of the graph
                or as <em>Module</em> at any other position in the graph. </p>
                <p><a href="https://www.openmodelica.org/">
                <strong>OpenModelica</strong></a>
                is an open-source Modelica-based modeling and simulation
                environment intended for industrial and academic usage.
                Modelica is an object-oriented, declarative, multi-domain
                modeling language for component-oriented modeling of complex
                systems, e.g., systems containing mechanical, electrical,
                electronic, hydraulic, thermal, control, electric power or
                process-oriented subcomponents.</p>
                """
                }

    def update_config(self, k: str, v: Any) -> None:
        # bug -> currently not working
        if k == 'Initialization_Model Path' or k == 'Initialization_Model Name' or k == 'Initialization_Dependent Libraries':
            BasicModule.update_config(self, k, v)
            self.model_path = self.config['Initialization_Model Path']
            self.model_path = self.model_path.replace("\\", "/")
            self.model_name = self.config['Initialization_Model Name']
            self.dep_libs = self.config['Initialization_Dependent Libraries']
            OMSim.__init__(self, self.model_path, self.model_name, self.dep_libs)
        else:
            BasicModule.update_config(self, k, v)

    def execute_once(self) -> None:
        """Helper function:
        On Event "Execute_once", this function simulates the initialized model
        and pushes resulting DataFrame
        to the designated channel.

        Not supported features (ToDo?):
        - Data not initialized in module configuration cannot be updated
            - Solution: Matching with all Parameters?
        - Defined output of the simulation cannot be changed
        - Meta data

        :param name: Name of the channel
        :param sender: Module, providing the data
        :param data: Provided data frame
        :return: None
        """
        if self.config["Framework_Executable Module"] is True:
            self.params = self.config["Simulation_Model Parameter"]
            self.def_out = self.config["Simulation_Outputs"]
            self.inputs = self.config["Simulation_Model Inputs"]
            self.simOpts = self.config["Simulation_Simulation Options"]
            self.simFlags = self.config["Simulation_Simulation Flags"]

            df = self.simulate(def_out=self.def_out, inputs=self.inputs,
                               params=self.params, sim_opts=self.simOpts,
                               simFlags=self.simFlags)

            for channel in self.config["channels_push"]:
                self._push_data(channel, df)

    def on_new_data(self, name: str, sender: SourceModule,
                    data: pandas.DataFrame) -> None:
        """Helper function:
        On Event "New Data", this function converts the Pandas DataFrame
        into valid Dicts for OMSim-Class.
        If column names match
        the keys of certain dicts,
        the values of this key will be updated,
        assuming only the first row of the column is relevant.
        Simulates the given Model with generated inputs
        and pushes resulting DataFrame
        to the designated channel.


        Not supported features (ToDo?):
        - Data not initialized in module configuration cannot be updated
            - Solution: Matching with all Parameters?
        - Defined output of the simulation cannot be changed
        - Meta data


        :param name: Name of the channel
        :param sender: Module, providing the data
        :param data: Provided data frame
        :return: None
        """
        self.def_out = self.config["Simulation_Outputs"]

        for col in data.columns:
            if col in self.inputs.keys():
                self.inputs[col] = data.at[0, col]

            if col in self.params.keys():
                self.params[col] = data.at[0, col]

            if col in self.simOpts.keys():
                self.simOpts[col] = data.at[0, col]

            if col in self.simFlags.keys():
                self.simFlags[col] = data.at[0, col]

        df = self.simulate(def_out=self.def_out, inputs=self.inputs,
                           params=self.params, sim_opts=self.simOpts,
                           simFlags=self.simFlags)

        for channel in self.config["channels_push"]:
                self._push_data(channel, df)
