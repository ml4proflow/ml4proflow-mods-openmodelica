from __future__ import annotations
import os
from typing import Any, Union
import pandas

from ml4proflow.modules import (Module,
                                DataFlowManager, SourceModule)
from .openmodelica import OMSim


class OMModule(Module, OMSim):
    """
    ml4proflow-module to perform simulations
    of modelica-models (.mo).
    This module receives
    from a previous module some simulation parameters
    (either inputs, parameters, options or flags)
    and pushes the output data from the simulation
    to the designated channel.
    """
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]) -> None:
        """
        Initialize the OMModule
        and converting the keys & values
        from the configuration file
        to simulation parameters.
        :param dataflow: Specifies the dataflow
        :param config: Configuration of the module
        :return: None
        """
        Module.__init__(self, dfm, config)

        # settings
        self.model_path = config.setdefault(
                                    'Initialization_Model Path',
                                    os.getcwd())
        self.model_path = self.model_path.replace("\\", "/")
        self.model_name = config.setdefault('Initialization_Model Name', "")
        self.params = config.setdefault("Simulation_Model Parameter", {})
        self.def_out = config.setdefault("Simulation_Outputs", [])

        # Expert settings
        self.dep_libs = config.setdefault('Initialization_Dependent Libraries',
                                          [])
        self.inputs = config.setdefault("Simulation_Model Inputs", {})
        self.simOpts = config.setdefault("Simulation_Simulation Options", {})
        self.simFlags = config.setdefault("Simulation_Simulation Flags", {})

        OMSim.__init__(self, self.model_path, self.model_name, self.dep_libs)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "Open Modelica Simulation",
                "categories": ["Simulation"],
                "jupyter-gui-cls": "ml4proflow_jupyter.widgets.BasicWidget",
                "jupyter-gui-config-hide": [
                             'Initialization_Dependent Libraries',
                             'Simulation_Model Inputs',
                             'Simulation_Simulation Options',
                             'Simulation_Simulation Flags'
                            ],
                "jupyter-gui-override-settings-type": {
                             'Initialization_Model Path': 'file',
                             'Initialization_Model Name': 'string',
                             'Simulation_Model Parameter': 'dict',
                             'Simulation_Outputs': 'list'
                            },
                "html-description": """
                <p>This <strong>ml4proflow module</strong> simulates
                modelica-models (.mo) with the python interface of
                OpenModelica. It can bes used as
                <em>ExecutableModule</em> at the beginning of the graph
                or as <em>Module</em> at any other position in the graph. </p>
                <p><a href="https://www.openmodelica.org/">
                <strong>OpenModelica</strong></a>
                is an open-source Modelica-based modeling and simulation
                environment intended for industrial and academic usage.
                Modelica is an object-oriented, declarative, multi-domain
                modeling language for component-oriented modeling of complex
                systems, e.g., systems containing mechanical, electrical,
                electronic, hydraulic, thermal, control, electric power or
                process-oriented subcomponents.</p>
                """
                }

    def on_new_data(self, name: str, sender: SourceModule,
                    data: pandas.DataFrame) -> None:
        """Helper function:
        On Event "New Data", this function converts the Pandas DataFrame
        into valid Dicts for OMSim-Class.
        If column names match
        the keys of certain dicts,
        the values of this key will be updated,
        assuming only the first row of the column is relevant.
        Simulates the given Model with generated inputs
        and pushes resulting DataFrame
        to the designated channel.


        Not supported features (ToDo?):
        - Data not initialized in module configuration cannot be updated
            - Solution: Matching with all Parameters?
        - Defined output of the simulation cannot be changed
        - Meta data


        :param name: Name of the channel
        :param sender: Module, providing the data
        :param data: Provided data frame
        :return: None
        """
        for col in data.columns:
            if col in self.inputs.keys():
                self.inputs[col] = data.at[0, col]

            if col in self.params.keys():
                self.params[col] = data.at[0, col]

            if col in self.simOpts.keys():
                self.simOpts[col] = data.at[0, col]

            if col in self.simFlags.keys():
                self.simFlags[col] = data.at[0, col]

        df = self.simulate(def_out=self.def_out, inputs=self.inputs,
                           params=self.params, sim_opts=self.simOpts,
                           simFlags=self.simFlags)

        self._push_data(self.config['channels_push'][0], df)
