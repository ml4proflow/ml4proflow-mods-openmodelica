import unittest
from ml4proflow import modules
from ml4proflow_mods.openmodelica.modules import OMModule


class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()

    def test_create_ommodule(self):
        dut = OMModule(self.dfm, {})
        self.assertIsInstance(dut, OMModule)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
